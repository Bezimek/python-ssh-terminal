import paramiko
import argparse
import sys
################################################################################################################
#                                                 FLAGS                                                        #
################################################################################################################
flags = argparse.ArgumentParser()

flags.add_argument("-host", "--hostname", default = "bandit.labs.overthewire.org", help = "Define the host you want to connect with.")
flags.add_argument("-u", "--username", default = "bandit0", help = "Define the user you want to log in")
flags.add_argument("-p", "--password", default = "bandit0", help = "Provide the password for user.")
flags.add_argument("-P", "--port", default = 2220, help = "Define the number of port you want to connect to")

args = flags.parse_args()
################################################################################################################
#                                                 FLAGS                                                        #
################################################################################################################


try:
    shell = paramiko.SSHClient() 
    shell.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # so the host is treated as known_host
    shell.connect(args.hostname , username = args.username, password = args.password, port = args.port)
    session = shell.get_transport().open_session()
except Exception as e:
    print("Provided credentials are invalid or the host is unreachable. Occured error:", e)
    sys.exit()

print("Connection established!")  
try:

    if shell._transport.get_banner():
        print(shell._transport.get_banner())
    else:
        print("Unable to load banner!")
    print("type 'exit' if you wish to disconnect")
    while session.active:           # For some reason it doesn't change the state, so use temporary 'exit' command
        command = input("user_input:")
        if command == "exit":
            shell.close()
            break
        stdin,stdout,stderr=shell.exec_command(command)
        print(*stdout.readlines(), *stderr.readlines())
except Exception as e:
    print("I have no idea, what happened, here is the error:", e)


